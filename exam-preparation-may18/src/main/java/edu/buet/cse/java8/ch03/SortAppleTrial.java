package edu.buet.cse.java8.ch03;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import edu.buet.cse.java8.model.Apple;
import edu.buet.cse.java8.model.Color;

public class SortAppleTrial {
  public static void main(String... args) {
    List<Apple> appleList = Arrays.asList(new Apple(80, Color.GREEN), new Apple(155, Color.GREEN),
        new Apple(160, Color.RED), new Apple(120, Color.RED), new Apple(75, Color.BROWN));

    appleList.sort((Apple a1, Apple a2) -> Integer.compare(a1.getWeight(), a2.getWeight()));
    appleList.stream().forEach(System.out::println);
    System.out.println();

    Collections.shuffle(appleList);
    appleList.sort(Comparator.comparing(Apple::getWeight));
    appleList.stream().forEach(System.out::println);
    System.out.println();
  }
}
