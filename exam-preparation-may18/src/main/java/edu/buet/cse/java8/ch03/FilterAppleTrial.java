package edu.buet.cse.java8.ch03;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import edu.buet.cse.java8.model.Apple;
import edu.buet.cse.java8.model.Color;

public class FilterAppleTrial {
  private static final int LOWER_THRESHOLD = 80;
  private static final int UPPER_THRESHOLD = 150;

  public static void main(String... args) {
    List<Apple> appleList = Arrays.asList(new Apple(80, Color.GREEN), new Apple(155, Color.GREEN),
        new Apple(160, Color.RED), new Apple(120, Color.RED), new Apple(75, Color.BROWN));

    // filter green apples
    appleList.stream().filter((Apple a) -> a.getColor() == Color.GREEN)
        .forEach(System.out::println);
    System.out.println();

    // filter heavy red apples
    List<Apple> heavyRedAppleList = appleList.stream().filter(a -> a.getColor() == Color.RED)
        .filter(a -> a.getWeight() > UPPER_THRESHOLD).collect(Collectors.toList());

    heavyRedAppleList.stream().forEach(System.out::println);
    System.out.println();

    // filter weird apples
    appleList.stream().filter(a -> a.getWeight() < LOWER_THRESHOLD && a.getColor() == Color.BROWN)
        .forEach(System.out::println);
  }
}
