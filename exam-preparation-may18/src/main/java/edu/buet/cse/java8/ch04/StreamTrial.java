package edu.buet.cse.java8.ch04;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import edu.buet.cse.java8.model.Dish;

public class StreamTrial {
  private static final int CALORY_THRESHOLD = 400;

  public static void main(String... args) {
    List<Dish> menu = Dish.getMenu();

    // filter low calorie dishes and print their names
    List<String> dishNameList =
        menu.parallelStream().filter(d -> d.getCalories() < CALORY_THRESHOLD)
            .sorted(Comparator.comparing(Dish::getCalories))
            .map(Dish::getName)
            .collect(Collectors.toList());

    dishNameList.stream().forEach(System.out::println);
  }
}
