package edu.buet.cse.java8.ch03;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import edu.buet.cse.java8.model.Apple;
import edu.buet.cse.java8.model.Color;

public class LambdaTrial {
  private static final int WEIGHT_THRESHOLD = 150;
  
  public static void main(String... args) {
    // create a runnable using a lambda expression
    Runnable r =
        () -> System.out.println("This is " + Thread.currentThread().getName() + " speaking !");
    Thread t = new Thread(r, "customThread");
    t.start();

    try {
      t.join();
    } catch (InterruptedException ex) {
      System.err.println(ex);
    }

    // create a comparator using a lambda expression
    List<Apple> appleList = Arrays.asList(new Apple(80, Color.GREEN), new Apple(155, Color.GREEN),
        new Apple(160, Color.RED), new Apple(120, Color.RED), new Apple(75, Color.BROWN));

    Comparator<Apple> comp1 = (a1, a2) -> Integer.compare(a1.getWeight(), a2.getWeight());
    appleList.sort(comp1);
    appleList.stream().forEach(System.out::println);
    System.out.println();

    // create a predicate using a lambda expression
    Predicate<Apple> greenPredicate = a -> a.getColor() == Color.GREEN;
    List<Apple> greenAppleList = filterApples(appleList, greenPredicate);
    greenAppleList.stream().forEach(System.out::println);
    System.out.println();

    List<Apple> heavyAppleList = filterApples(appleList, LambdaTrial::isHeavy);
    heavyAppleList.stream().forEach(System.out::println);
  }

  private static List<Apple> filterApples(List<Apple> appleList, Predicate<Apple> p) {
    return appleList.stream().filter(p).collect(Collectors.toList());
  }

  private static boolean isHeavy(Apple a) {
    return a.getWeight() > WEIGHT_THRESHOLD;
  }
}
