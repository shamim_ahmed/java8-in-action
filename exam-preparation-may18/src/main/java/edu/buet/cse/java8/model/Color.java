package edu.buet.cse.java8.model;

public enum Color {
  RED, GREEN, BROWN
}
