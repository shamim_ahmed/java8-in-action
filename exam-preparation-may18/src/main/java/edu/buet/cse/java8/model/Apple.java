package edu.buet.cse.java8.model;

public class Apple {
  private final int weight;
  private final Color color;
  
  public Apple(int weight, Color color) {
    this.weight = weight;
    this.color = color;
  }

  public int getWeight() {
    return weight;
  }

  public Color getColor() {
    return color;
  }
  
  @Override
  public String toString() {
    return String.format("Apple[weight = %d, color = %s]", weight, color);
  }
}
