package edu.buet.cse.ocpjp;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JdbcTrial {
  private static final String DB_URL = "jdbc:mysql://localhost:3306/ocjp_db";

  public static void main(String... args) {
    Properties props = new Properties();
    props.put("user", "root");
    props.put("password", "admin");

    try (Connection conn = DriverManager.getConnection(DB_URL, props)) {
      DatabaseMetaData metaData = conn.getMetaData();
      String driverName = metaData.getDriverName();
      String driverVersion = metaData.getDriverVersion();

      System.out.println("Driver name: " + driverName);
      System.out.println("Driver version: " + driverVersion);
    } catch (SQLException ex) {
      System.err.println(ex);
    }
  }
}
