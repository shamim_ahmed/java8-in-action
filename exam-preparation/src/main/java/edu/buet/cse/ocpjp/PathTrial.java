package edu.buet.cse.ocpjp;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathTrial {
  public static void main(String... args) {
    Path path = Paths.get("/home/shamim/codes/bitbucket-projects/java8-in-action/exam-preparation/pom.xml");
    System.out.println(path.subpath(0, 2));
  }
}
