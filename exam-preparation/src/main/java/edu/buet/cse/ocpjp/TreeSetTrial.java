package edu.buet.cse.ocpjp;

import java.util.TreeSet;

public class TreeSetTrial {
  public static void main(String... args) {
    TreeSet<Integer> valueSet = new TreeSet<>();

    for (int i = 320; i <= 350; i++) {
      if (i % 2 == 0) {
        valueSet.add(i);
      }
    }

    TreeSet<Integer> valueSubSet = (TreeSet<Integer>) valueSet.subSet(320, true, 328, true);
    // the following line will throw an exception
    valueSubSet.add(329);
    System.out.println(valueSubSet);
  }
}
