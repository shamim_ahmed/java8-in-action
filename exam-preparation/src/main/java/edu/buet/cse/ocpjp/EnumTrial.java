package edu.buet.cse.ocpjp;

public class EnumTrial {
  enum Season {
    SUMMER, AUTUMN, WINTER, SPRING
  }

  public static void main(String... args) {
    // usage of valueOf method
    Season season = Season.valueOf("SUMMER");
    System.out.println(season);

    // usage of values method
    Season[] seasonArray = Season.values();

    for (Season s : seasonArray) {
      System.out.println(s);
    }
  }
}
