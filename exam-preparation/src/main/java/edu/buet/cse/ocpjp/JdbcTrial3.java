package edu.buet.cse.ocpjp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcTrial3 {
  private static final String DB_URL = "jdbc:mysql://localhost:3306/ocjp_db";
  private static final String DB_QUERY = "SELECT * FROM Product";

  public static void main(String... args) {
    try (Connection conn = DriverManager.getConnection(DB_URL, "root", "admin");
        Statement stmt = conn.createStatement()) {
      ResultSet resultSet = stmt.executeQuery(DB_QUERY);
      ResultSetMetaData metaData = resultSet.getMetaData();
      int columnCount = metaData.getColumnCount();
      System.out.printf("Column count: %d%n%n", columnCount);

      for (int i = 1; i <= columnCount; i++) {
        System.out.println("Column index: " + i);
        System.out.println("Column name: " + metaData.getColumnName(i));
        System.out.println("Column type: " + metaData.getColumnTypeName(i));
        System.out.println();
      }
    } catch (SQLException ex) {
      System.err.println(ex);
    }
  }
}
