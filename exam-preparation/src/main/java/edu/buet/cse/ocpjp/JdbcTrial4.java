package edu.buet.cse.ocpjp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class JdbcTrial4 {
  private static final String DB_URL = "jdbc:mysql://localhost:3306/ocjp_db";
  private static final String DB_QUERY = "SELECT * FROM User WHERE email = ?";

  public static void main(String... args) {
    Properties props = new Properties();
    props.put("user", "root");
    props.put("password", "admin");
    try (Connection conn = DriverManager.getConnection(DB_URL, props);
        PreparedStatement stmt = conn.prepareStatement(DB_QUERY)) {
      stmt.setString(1, "derek@gmail.com");
      ResultSet resultSet = stmt.executeQuery();

      while (resultSet.next()) {
        System.out.println(resultSet.getString("email"));
      }
    } catch (SQLException ex) {
      System.err.println(ex);
    }
  }
}
