package edu.buet.cse.ocpjp;

public class Movie {
  static enum Genre {
    HORROR, THRILLER, COMEDY, TRAGEDY
  }

  private final String name;
  private final Genre genre;
  private final char rating;

  public Movie(String name, Genre genre, char rating) {
    this.name = name;
    this.genre = genre;
    this.rating = rating;
  }

  public String getName() {
    return name;
  }

  public Genre getGenre() {
    return genre;
  }

  public char getRating() {
    return rating;
  }
}
