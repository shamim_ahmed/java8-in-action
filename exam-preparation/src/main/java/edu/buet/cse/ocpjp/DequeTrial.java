package edu.buet.cse.ocpjp;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;

public class DequeTrial {
  public static void main(String... args) {
    Deque<Integer> deque = new ArrayDeque<>();
    
    List<Integer> valueList = Arrays.asList(1, 2, 3);
    valueList.stream().forEach(i -> deque.push(i));
    
    while (!deque.isEmpty()) {
      System.out.println(deque.remove());
    }
  }
}
