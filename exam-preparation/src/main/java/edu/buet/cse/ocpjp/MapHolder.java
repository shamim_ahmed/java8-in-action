package edu.buet.cse.ocpjp;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MapHolder<K, V> {
  private Map<K, V> map = new HashMap<>();
  private ReadWriteLock lock = new ReentrantReadWriteLock();

  public V get(K key) {
    try {
      lock.readLock().lock();
      return map.get(key);
    } finally {
      lock.readLock().unlock();
    }
  }

  public void put(K key, V value) {
    try {
      lock.writeLock().lock();
      map.put(key, value);
    } finally {
      lock.writeLock().unlock();
    }
  }
}
