package edu.buet.cse.ocpjp;

import java.util.Arrays;
import java.util.List;

public class FilterTrial7 {
  public static void main(String... args) {
    List<Character> ratingList = Arrays.asList('R', 'U', 'A');
    
    ratingList.stream()
    .filter(x -> x == 'R')
    .peek(x -> System.out.println("Old rating: " + x))
    .map(x -> x == 'R' ? 'A' : x)
    .forEach(x -> System.out.println("New rating: " + x));
  }
}
