package edu.buet.cse.ocpjp;

public class LockTrial {
  public static void main(String... args) {
    MapHolder<Integer, Integer> holder = new MapHolder<>();
    int max = 5;

    Runnable producer = () -> {
      for (int i = 1; i <= max; i++) {
        holder.put(i, i * i);

        try {
          Thread.sleep(1000);
        } catch (InterruptedException ex) {
          System.err.println(ex);
        }
      }
    };

    Runnable consumer = () -> {
      for (int i = 1; i <= max; i++) {
        Integer val = holder.get(i);
        System.out.println(i + " " + val);

        try {
          Thread.sleep(1500);
        } catch (InterruptedException ex) {
          System.err.println(ex);
        }
      }
    };

    new Thread(producer).start();
    new Thread(consumer).start();
  }
}
