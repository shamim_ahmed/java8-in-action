package edu.buet.cse.ocpjp;

import java.util.Arrays;
import java.util.List;

public class FilterTrial2 {
  public static void main(String... args) {
    List<Movie> movieList = Arrays.asList(new Movie("Titanic", Movie.Genre.TRAGEDY, 'U'),
        new Movie("Psycho", Movie.Genre.THRILLER, 'U'),
        new Movie("Oldboy", Movie.Genre.COMEDY, 'R'),
        new Movie("Shining", Movie.Genre.HORROR, 'R'));

    // figure out why the following code will not print anything
    movieList.stream()
        .filter(movie -> movie.getRating() == 'R')
        .peek(movie -> System.out.println(movie.getName()))
        .map(movie -> movie.getGenre());
  }
}
