package edu.buet.cse.ocpjp;

import java.io.IOException;

public class Device implements AutoCloseable {
  private String name;

  public Device(String name) throws IOException {
    this.name = name;

    if ("D2".equals(name)) {
      throw new IOException("Invalid device name: " + name);
    }
  }

  public String read() throws IOException {
    return "";
  }

  @Override
  public void close() throws Exception {
    System.out.println("Closing device: " + name);
    throw new RuntimeException("RTE thrown for device: " + name);
  }
}
