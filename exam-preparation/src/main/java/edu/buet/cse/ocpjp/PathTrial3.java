package edu.buet.cse.ocpjp;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathTrial3 {
  public static void main(String... args) {
    Path p1 = Paths.get("/home/shamim/personal/temp/./ocpjp/../hello.txt");
    Path p2 = Paths.get("/home/shamim/personal/temp/poem.txt");
    Path p3 = p1.relativize(p2);
    System.out.println(p3);
  }
}
