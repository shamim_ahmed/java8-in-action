package edu.buet.cse.ocpjp;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class FilterTrial3 {
  public static void main(String... args) {
    List<Movie> movieList = Arrays.asList(new Movie("Titanic", Movie.Genre.TRAGEDY, 'U'),
        new Movie("Psycho", Movie.Genre.THRILLER, 'U'),
        new Movie("Oldboy", Movie.Genre.THRILLER, 'R'),
        new Movie("Shining", Movie.Genre.HORROR, 'R'));

    Predicate<Movie> thrillerPredicate = movie -> movie.getGenre() == Movie.Genre.THRILLER;
    Predicate<Movie> namePredicate = movie -> movie.getName().startsWith("O");
    
    movieList.stream()
    .filter(thrillerPredicate)
    .filter(namePredicate)
    .forEach(movie -> System.out.println(movie.getName()));
  }
}
