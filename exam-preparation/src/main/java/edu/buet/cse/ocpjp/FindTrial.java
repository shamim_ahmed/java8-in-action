package edu.buet.cse.ocpjp;

import java.util.Arrays;
import java.util.List;

public class FindTrial {
  public static void main(String... args) {
    List<String> valueList = Arrays.asList("Alpha A", "Alpha B", "Alpha C");
    
    // this is guaranteed to be true
    boolean flag1 = valueList.stream().findFirst().get().equals("Alpha A");
    System.out.println(flag1);
   
    // there is no guarantee that this will be true
    boolean flag2 = valueList.stream().findAny().get().equals("Alpha A");
    System.out.println(flag2);
  }
}
