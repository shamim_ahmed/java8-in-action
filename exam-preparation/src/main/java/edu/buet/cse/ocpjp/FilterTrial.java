package edu.buet.cse.ocpjp;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FilterTrial {
  public static void main(String... args) {
    List<Movie> movieList = Arrays.asList(new Movie("Titanic", Movie.Genre.TRAGEDY, 'U'),
        new Movie("Psycho", Movie.Genre.THRILLER, 'U'),
        new Movie("Oldboy", Movie.Genre.COMEDY, 'R'),
        new Movie("Shining", Movie.Genre.HORROR, 'R'));

    List<String> nameList = movieList.stream()
        .filter(movie -> movie.getRating() == 'R')
        .map(movie -> movie.getName())
        .collect(Collectors.toList());

    nameList.stream()
    .forEach(s -> System.out.println(s));
  }
}
