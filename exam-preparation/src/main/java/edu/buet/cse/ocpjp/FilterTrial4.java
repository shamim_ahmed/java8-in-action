package edu.buet.cse.ocpjp;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class FilterTrial4 {
  public static void main(String... args) {
    List<Integer> valueList = Arrays.asList(2, 4, 5, 7, 8);
    Predicate<Integer> pred1 = (Integer i) -> {
      System.out.println("value is: " + i);
      return i.intValue() == 4;
    };
    Predicate<Integer> pred2 = (Integer i) -> i % 2 == 0;
    
    // execute several times and note the order of the output !
    long n = valueList.parallelStream().filter(pred1).filter(pred2).count();
    System.out.println(n);
  }
}
