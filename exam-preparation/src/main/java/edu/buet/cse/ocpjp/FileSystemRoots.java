package edu.buet.cse.ocpjp;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class FileSystemRoots {
  public static void main(String... args) {
    // note the return type here
    Iterable<Path> rootDirectories = FileSystems.getDefault().getRootDirectories();

    for (Path rootDir : rootDirectories) {
      System.out.println(rootDir);
    }
    
    // note the return type here
    File[] roots = File.listRoots();
    
    for (File r : roots) {
      System.out.println(r);
    }   
  }
}
