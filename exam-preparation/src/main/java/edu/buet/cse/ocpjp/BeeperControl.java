package edu.buet.cse.ocpjp;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class BeeperControl {
  private ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);

  public void scheduleBeeper() {
    ScheduledFuture<?> beeper = executorService
        .scheduleAtFixedRate(() -> System.out.println("beep"), 5, 10, TimeUnit.SECONDS);
    executorService.schedule(() -> {
      beeper.cancel(true);
      executorService.shutdown();
    }, 60, TimeUnit.SECONDS);
  }

  public static void main(String... args) {
    BeeperControl beeperControl = new BeeperControl();
    beeperControl.scheduleBeeper();
  }
}
