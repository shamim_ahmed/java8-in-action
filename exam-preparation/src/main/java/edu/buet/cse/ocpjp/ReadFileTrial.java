package edu.buet.cse.ocpjp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ReadFileTrial {
  public static void main(String... args) {
    try {
      List<String> lines = Files.readAllLines(Paths.get("/home/shamim/personal/temp/poem.txt"));
      lines.stream().forEach(s -> System.out.println(s));
    } catch (IOException ex) {
      System.err.println(ex);
    }
  }
}
