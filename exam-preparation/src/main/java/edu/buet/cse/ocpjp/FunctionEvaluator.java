package edu.buet.cse.ocpjp;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FunctionEvaluator {
  public static void main(String... args) {
    List<Double> inputList = Arrays.asList(0.984, 1.244, 2.727, 2.804);
    List<Double> resultList = inputList.stream().map(x -> evaluate(x)).collect(Collectors.toList());

    for (int i = 0, n = inputList.size(); i < n; i++) {
      System.out.println(inputList.get(i) + " -> " + resultList.get(i));
    }
  }

  private static double evaluate(double x) {
    return 9 * Math.pow(x, 2) - Math.pow(3.0, x + 1) * Math.log(3) - 1;
  }
}
