package edu.buet.cse.ocpjp;

public class DeviceTrial {
  public static void main(String... args) {
    try (Device d1 = new Device("D1"); Device d2 = new Device("D2")) {
      throw new Exception("test");
    } catch (Exception ex) {
      System.out.println(ex.getClass().getCanonicalName() + " : " + ex.getMessage());
      Throwable[] suppressedArray = ex.getSuppressed();

      for (Throwable t : suppressedArray) {
        System.out.println(t.getClass().getCanonicalName() + " : " + t.getMessage());
      }
    }
  }
}
