package edu.buet.cse.ocpjp;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathTrial5 {
  public static void main(String... args) {
    Path p1 = Paths.get("docs/index.html");
    Path p2 = Paths.get("/home");
    Path p3 = p1.relativize(p2);
    System.out.println(p3);
  }
}
