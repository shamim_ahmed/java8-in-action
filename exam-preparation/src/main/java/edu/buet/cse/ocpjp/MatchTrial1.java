package edu.buet.cse.ocpjp;

import java.util.Arrays;
import java.util.List;

public class MatchTrial1 {
  public static void main(String... args) {
    List<String> languageList = Arrays.asList("C#", "Python", "Java");

    boolean flag = languageList.stream().allMatch(s -> {
      System.out.println(s);
      return s.equals("Java");
    });
    
    System.out.println(flag);
  }
}
