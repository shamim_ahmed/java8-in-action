package edu.buet.cse.ocpjp;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathTrial4 {
  public static void main(String... args) {
    Path p1 = Paths.get("/home/shamim/hello.txt");
    Path p2 = Paths.get("/tmp");
    Path p3 = p1.relativize(p2);
    System.out.println(p3);
  }
}
