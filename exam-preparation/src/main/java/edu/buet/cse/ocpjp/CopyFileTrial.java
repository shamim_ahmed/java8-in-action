package edu.buet.cse.ocpjp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class CopyFileTrial {
  public static void main(String... args) {
    try {
      List<String> lines = Files.readAllLines(Paths.get("/home/shamim/personal/temp/poem.txt"));
      Path target = Paths.get("/home/shamim/personal/temp/poem2.txt");

      lines.stream().forEach(s -> {
        try {
          Files.write(target, s.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException ex) {
          System.err.println("Error while writing to target: " + ex);
        }
      });
    } catch (IOException ex) {
      System.err.println(ex);
    }
  }
}
