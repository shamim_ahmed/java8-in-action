package edu.buet.cse.ocpjp;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcTrial2 {
  private static final String DB_URL = "jdbc:mysql://localhost:3306/ocjp_db";

  public static void main(String... args) {
    try (Connection conn = DriverManager.getConnection(DB_URL, "root", "admin")) {
      DatabaseMetaData metaData = conn.getMetaData();
      String dbName = metaData.getDatabaseProductName();
      String dbVersion = metaData.getDatabaseProductVersion();

      System.out.println("Database name: " + dbName);
      System.out.println("Database version: " + dbVersion);
    } catch (SQLException ex) {
      System.err.println(ex);
    }
  }
}
