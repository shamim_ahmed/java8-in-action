package edu.buet.cse.ocpjp;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathTrial2 {
  public static void main(String... args) {
    Path p1 = Paths.get("/home/shamim/personal/temp/./ocpjp/../hello.txt");
    Path p2 = p1.normalize();
    System.out.println(p2);
  }
}
