package edu.buet.cse.ch03.examples;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class LambdaDemo {
  private static final String RED = "red";
  private static final String GREEN = "green";

  public static void main(String... args) {
    // create a Runnable instance using lambda
    Runnable r = () -> System.out.println("Hello World !");
    new Thread(r).start();

    // sort apples by weight and print 
    List<Apple> appleList =
        Arrays.asList(new Apple(80, GREEN), new Apple(155, GREEN), new Apple(120, RED));
    Comparator<Apple> appleComparator =
        (Apple a1, Apple a2) -> Integer.compare(a1.getWeight(), a2.getWeight());
    appleList.sort(appleComparator);
    appleList.stream().forEach(System.out::println);
  }
}
