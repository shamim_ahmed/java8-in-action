package edu.buet.cse.ch03.examples;

public interface ApplePredicate {
  boolean test(Apple apple);
}
