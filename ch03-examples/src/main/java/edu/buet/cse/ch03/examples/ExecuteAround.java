package edu.buet.cse.ch03.examples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ExecuteAround {
  private static final String INPUT_FILE = "/data.txt";

  public static void main(String... args) throws IOException {
    String output1 = processFileLimited();
    System.out.println(output1);

    String output2 = processFile((BufferedReader r) -> r.readLine());
    System.out.println(output2);

    String output3 =
        processFile((BufferedReader r) -> String.format("%s, %s", r.readLine(), r.readLine()));
    System.out.println(output3);
  }

  private static String processFileLimited() throws IOException {
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(ExecuteAround.class.getResourceAsStream(INPUT_FILE)))) {
      return reader.readLine();
    }
  }

  private static String processFile(BufferedReaderProcessor p) throws IOException {
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(ExecuteAround.class.getResourceAsStream(INPUT_FILE)))) {
      return p.process(reader);
    }
  }
}
