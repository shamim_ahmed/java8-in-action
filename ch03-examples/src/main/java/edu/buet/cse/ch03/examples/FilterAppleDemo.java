package edu.buet.cse.ch03.examples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FilterAppleDemo {
  private static final String RED = "red";
  private static final String GREEN = "green";
  
  public static void main(String...args) {
    List<Apple> appleList =
        Arrays.asList(new Apple(80, GREEN), new Apple(155, GREEN), new Apple(120, RED));
    
    // filter red apples and print
    List<Apple> redAppleList = filter(appleList, (Apple a) -> RED.equals(a.getColor()));
    redAppleList.stream().forEach(System.out::println);
  }
  
  private static List<Apple> filter(List<Apple> appleList, ApplePredicate predicate) {
    List<Apple> resultList = new ArrayList<>();
    
    for (Apple apple : appleList) {
      if (predicate.test(apple)) {
        resultList.add(apple);
      }
    }
    
    return resultList;
  }
}
