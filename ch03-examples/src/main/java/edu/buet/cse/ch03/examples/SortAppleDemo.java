package edu.buet.cse.ch03.examples;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Comparator.comparing;

public class SortAppleDemo {
  private static final String RED = "red";
  private static final String GREEN = "green";

  public static void main(String... args) {
    List<Apple> appleList =
        Arrays.asList(new Apple(80, GREEN), new Apple(155, GREEN), new Apple(120, RED));

    // sort apples by weight and print
    appleList.sort((Apple a1, Apple a2) -> Integer.compare(a1.getWeight(), a2.getWeight()));
    appleList.stream().forEach(System.out::println);

    // shuffle the sorted list
    Collections.shuffle(appleList);
    System.out.println("\n--------------------\n");

    // sort again using a different syntax
    appleList.sort(comparing(Apple::getWeight));
    appleList.stream().forEach(System.out::println);
  }
}
