package edu.buet.cse.ch03.examples;

public class Apple {
  private int weight;
  private String color;

  public Apple(int weight, String color) {
    this.weight = weight;
    this.color = color;
  }

  public int getWeight() {
    return weight;
  }

  public void setWeight(int weight) {
    this.weight = weight;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("Apple[").append("weight = ").append(weight).append(", color = ").append(color)
        .append("]");
    return sb.toString();
  }
}
