package edu.buet.cse.ch03.examples;

import java.io.BufferedReader;
import java.io.IOException;

public interface BufferedReaderProcessor {
  String process(BufferedReader reader) throws IOException;
}
