package edu.buet.cse.ch01.examples;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class FilterAppleDemo {
  private static final String RED = "red";
  private static final String GREEN = "green";
  private static final String BROWN = "brown";
  private static final int LOWER_THRESHOLD = 80;
  private static final int UPPER_THRESHOLD = 150;

  public static void main(String... args) {
    List<Apple> appleList =
        Arrays.asList(new Apple(80, GREEN), new Apple(155, GREEN), new Apple(120, RED));

    // filter red apples and print
    System.out.println("-----Red Apples-----");

    List<Apple> redAppleList =
        appleList.stream().filter(a -> RED.equals(a.getColor())).collect(toList());
    redAppleList.stream().forEach(System.out::println);

    System.out.println();

    // filter green apples and print
    System.out.println("-----Green Apples-----");

    List<Apple> greenAppleList =
        appleList.stream().filter((Apple a) -> GREEN.equals(a.getColor())).collect(toList());
    greenAppleList.stream().forEach(System.out::println);

    System.out.println();

    // filter heavy apples and print
    System.out.println("-----Heavy Apples-----");

    appleList.stream().filter(FilterAppleDemo::isHeavyApple).forEach((Apple a) -> {
      System.out.println(a);
    });

    System.out.println();

    // filter weird apples and print
    System.out.println("-----Weird Apples-----");

    List<Apple> weirdAppleList = appleList.stream()
        .filter((Apple a) -> BROWN.equals(a.getColor()) || a.getWeight() < LOWER_THRESHOLD)
        .collect(toList());
    weirdAppleList.stream().forEach(System.out::println);

    System.out.println();
  }

  private static boolean isHeavyApple(Apple apple) {
    return apple.getWeight() > UPPER_THRESHOLD;
  }
}
