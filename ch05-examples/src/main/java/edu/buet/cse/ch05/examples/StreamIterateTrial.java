package edu.buet.cse.ch05.examples;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamIterateTrial {
  public static void main(String... args) {
    // construct a stream of 10 even numbers
    System.out.println("\n--- Even Numbers ---");

    List<Integer> evenNumbers = Stream.iterate(0, n -> n + 2)
                                      .limit(10)
                                      .collect(Collectors.toList());

    evenNumbers.stream().forEach(System.out::println);

    // generate first 10 fibonacci numbers
    System.out.println("\n--- Fibonacci Numbers ---");

    Stream.iterate(new int[] {0, 1}, t -> new int[] {t[1], t[0] + t[1]})
          .limit(10).map(t -> t[0])
          .forEach(System.out::println);
  }
}
