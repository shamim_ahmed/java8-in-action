package edu.buet.cse.ch05.examples;

import java.util.function.IntSupplier;
import java.util.stream.IntStream;

public class FibonacciUsingSupplier {
  public static void main(String... args) {
    IntStream.generate(new FibonacciSupplier())
              .limit(10)
              .forEach(System.out::println);
  }

  private static class FibonacciSupplier implements IntSupplier {
    private int x = 0;
    private int y = 1;

    @Override
    public int getAsInt() {
      int result = x;
      x = y;
      y = result + y;

      return result;
    }
  }
}
