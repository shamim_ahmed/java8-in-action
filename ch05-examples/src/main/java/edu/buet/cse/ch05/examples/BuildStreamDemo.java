package edu.buet.cse.ch05.examples;

import java.util.Arrays;
import java.util.OptionalDouble;
import java.util.stream.Stream;

public class BuildStreamDemo {

  public static void main(String... args) {
    Stream<String> wordStream = Stream.of("Java8", "Lambdas", "in", "action");
    wordStream.map(String::toUpperCase).forEach(System.out::println);

    // empty stream
    Stream<Integer> emptyStream = Stream.empty();
    emptyStream.forEach(System.out::println);

    // stream from array
    int[] values = {1, 2, 3, 4, 5};
    OptionalDouble averageVal = Arrays.stream(values).average();

    if (averageVal.isPresent()) {
      System.out.println("The average value is = " + averageVal.getAsDouble());
    }
  }
}
