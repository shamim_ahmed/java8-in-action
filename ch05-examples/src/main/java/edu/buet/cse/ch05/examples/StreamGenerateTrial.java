package edu.buet.cse.ch05.examples;

import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamGenerateTrial {
  public static void main(String... args) {
    // random stream of doubles
    System.out.println("--- 10 random doubles ---");

    Stream.generate(Math::random)
          .limit(10)
          .forEach(System.out::println);

    // generate a stream of 1s
    System.out.println("--- Stream of 1s ---");

    Stream.generate(() -> 1)
          .limit(5)
          .forEach(System.out::println);

    // generate a stream of 2s
    System.out.println("--- Stream of 2s ---");

    Stream.generate(new Supplier<Integer>() {
      @Override
      public Integer get() {
        return 2;
      }
    }).limit(5).forEach(System.out::println);

    // generate a stream of 3s
    System.out.println("--- Stream of 3s ---");

    IntStream.generate(new IntSupplier() {
      @Override
      public int getAsInt() {
        return 3;
      }
    }).limit(5).forEach(System.out::println);
  }
}
