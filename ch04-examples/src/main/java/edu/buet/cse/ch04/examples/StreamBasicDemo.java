package edu.buet.cse.ch04.examples;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamBasicDemo {
  private static final int CALORIE_THRESHOLD = 400;

  public static void main(String... args) {
    // find and print all low calorie dishes
    System.out.println("--- Low calorie Dishes ---");

    List<String> lowCaloryDishNames =
        Dish.menu.stream().filter((Dish d) -> d.getCalories() < CALORIE_THRESHOLD)
            .sorted(Comparator.comparing(Dish::getCalories)).map(Dish::getName)
            .collect(Collectors.toList());

    lowCaloryDishNames.stream().forEach(System.out::println);

    // find and print all fish dishes
    System.out.println("\n--- Fish dishes ---");

    Dish.menu.stream().filter(d -> d.getType() == Dish.Type.FISH).map(Dish::getName)
        .forEach(System.out::println);

    // find and print the name and calorie of each vegetarian dish
    System.out.println("\n--- Vegetarian dishes (sequential) ---");
    Dish.menu.stream().filter(Dish::isVegetarian).forEach(StreamBasicDemo::printDish);

    // print the vegetarian dishes again,but this time use a parallel stream
    System.out.println("\n--- Vegetarian dishes (parallel stream) ---");
    Dish.menu.parallelStream().filter(Dish::isVegetarian).forEach(StreamBasicDemo::printDish);

    // print all meat dishes sorted by their name
    System.out.println("\n--- Meat dishes(sorted by name) ---");

    Dish.menu.stream().filter(d -> d.getType() == Dish.Type.MEAT)
        .sorted(Comparator.comparing(Dish::getName)).map(Dish::getName)
        .forEach(System.out::println);

    // find if all meat dishes are of high calorie
    boolean meatDishCalorieFlag = Dish.menu.stream().filter(d -> d.getType() == Dish.Type.MEAT)
        .allMatch(d -> d.getCalories() >= CALORIE_THRESHOLD);

    System.out.printf("\nDo all meat dishes have calorie higher than the threashold ? -> %b%n",
        meatDishCalorieFlag);
  }

  private static void printDish(Dish dish) {
    System.out.printf("%s -> %d%n", dish.getName(), dish.getCalories());
  }
}
