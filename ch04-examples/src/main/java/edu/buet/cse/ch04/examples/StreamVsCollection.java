package edu.buet.cse.ch04.examples;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamVsCollection {

  public static void main(String... args) {
    List<String> words = Arrays.asList("Java8", "Streams", "In", "Action");
    Stream<String> s = words.stream();
    s.forEach(System.out::println);
  }
}
